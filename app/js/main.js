function menu(){
    const burger = document.querySelector('.hamburger');
    const header_menu = document.querySelector('.nav__menu');
    const body = document.querySelector('body');
    burger.addEventListener('click',  () => {
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
}

menu();

function dropdown(){
    const selectButtons = document.querySelectorAll('.dropdown__btn');
    const options = document.querySelectorAll('.dropdown__item');
    const allDropdown = document.querySelectorAll('.dropdown');
    for(const select of selectButtons){
        select.addEventListener('click', function(){
            this.closest('.dropdown').classList.toggle('active');
        });
    }

    for(const opt of options){
        opt.addEventListener('click', function(){
            const dropdown = this.closest('.dropdown');
            if(!this.classList.contains('selected')){
                this.parentNode.querySelector('.dropdown__item.selected')?.classList.remove('selected');
                this.classList.add('selected');
                dropdown.querySelector('.dropdown__btn-txt').innerText = this.querySelector('.dropdown__item-txt').textContent;
            }
            dropdown.classList.remove('active');
        });
    }

    window.addEventListener('load', function(){
        for(const dropdown of allDropdown){
            const items = dropdown.querySelectorAll('.dropdown__item');
            //const dropdownBtn = dropdown.querySelector('.dropdown__btn');
            const btnTxt = dropdown.querySelector('.dropdown__btn-txt');
            //const dropdownList = dropdown.querySelector('.dropdown__list');

            if([...items].every(e => !e.classList.contains('selected'))){
                btnTxt.innerText = items[0].querySelector('.dropdown__item-txt').textContent;
                items[0].classList.add('selected');
            }else{
                btnTxt.innerText = dropdown.querySelector('.dropdown__item.selected').textContent;
            }
            //dropdownList.style.width = dropdownBtn.offsetWidth + 'px';
            //dropdownList.style.top = dropdownBtn.offsetHeight + 10 + 'px';
        }
        setDropdownListPos();
    })
    //window.addEventListener('resize')

    window.addEventListener('click', function(evt){
        for(const dropdown of allDropdown){
            if(!dropdown.contains(evt.target)){
                dropdown.classList.remove('active');
            }
        }
    });
}

dropdown();

function setDropdownListPos() {
    const allDropdown = document.querySelectorAll('.dropdown');
    for(const dropdown of allDropdown){
        const dropdownBtn = dropdown.querySelector('.dropdown__btn');
        const dropdownList = dropdown.querySelector('.dropdown__list');
        dropdownList.style.top = dropdownBtn.offsetHeight + 10 + 'px';
    }
}

setDropdownListPos();

function handleGoodsBtn(){
    const addBtns = document.querySelectorAll('.card__add-btn');
    const removeBtns = document.querySelectorAll('.card__remove-btn');
    const goodsLimit = 10;

    addBtns.forEach(e => e.addEventListener('click', function(){
        const input = this.parentElement.querySelector('.card__goods-total');
        if(
            input.value === '' ||
            isNaN(parseInt(input.value))
        ){
            input.value = 1;
            this.parentElement.querySelector('.card__remove-btn').disabled = true;
            this.parentElement.querySelector('.card__add-btn').disabled = false;
        }else if(parseInt(input.value) + 1 >= goodsLimit){
            this.parentElement.querySelector('.card__add-btn').disabled = true;
            this.parentElement.querySelector('.card__remove-btn').disabled = false;
            input.value = goodsLimit;
        }else{
            input.value = parseInt(input.value) + 1;
            this.parentElement.querySelector('.card__add-btn').disabled = false;
            this.parentElement.querySelector('.card__remove-btn').disabled = false;
        }
    }));

    removeBtns.forEach(e => e.addEventListener('click', function(){
        const input = this.parentElement.querySelector('.card__goods-total');
        if(
            isNaN(parseInt(input.value)) ||
            input.value === '' ||
            parseInt(input.value) - 1 <= 1
        ){
            input.value = 1;
            this.parentElement.querySelector('.card__remove-btn').disabled = true;
            this.parentElement.querySelector('.card__add-btn').disabled = false;
        }else{
            input.value = parseInt(input.value) - 1;
            this.parentElement.querySelector('.card__add-btn').disabled = false;
        }
    }));

}


handleGoodsBtn();

function toggleAccordion(){
    const acc = document.getElementsByClassName("accordion__header");
    const panels = document.querySelectorAll('.accordion__body');


    [...acc].forEach((accordion, accIdx) => {
        accordion.addEventListener('click', function(){
            panels.forEach((panel, panelIdx) => {
                if(panelIdx !== accIdx){
                    panel.style.maxHeight = null;
                    panel.previousElementSibling.classList.remove('active');
                }else{
                    panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
                    panel.previousElementSibling.classList.toggle('active');
                }
            })
        });
    });
}

toggleAccordion();

function translateFormLabel(){

    const inputs = document.getElementsByClassName('form__input');

    [...inputs].forEach(e => e.addEventListener('focus', function(){
        this.nextElementSibling.classList.add('active');
    }));

    [...inputs].forEach(e => e.addEventListener('focusout', function(){
        if(this.value === ''){
            this.nextElementSibling.classList.remove('active');
        }
    }));
}

translateFormLabel();

function toggleDisabledBtn() {
    const checkbox = document.querySelector('.form__checkbox');
    const disabledBtn = document.querySelector('.form__submit-btn--subscription');
    checkbox.addEventListener('change', function(){
        disabledBtn.disabled = !this.checked;
    });
}

toggleDisabledBtn();

function initLightbox(){
    const showAllBtn = document.querySelectorAll('.img-block__show-all');
    showAllBtn.forEach(e => {
        e.addEventListener('click', function () {
            const lightboxUniqueId =
                this.parentElement.querySelector('.gallery').querySelector('a').getAttribute('data-fslightbox');
            fsLightboxInstances[lightboxUniqueId].open();
        })
    });
}
initLightbox();


function checkGoodsVal(el){
    if(parseInt(el.value) >= 10){
        el.value = 10;
        el.parentElement.querySelector('.card__add-btn').disabled = true;
        el.parentElement.querySelector('.card__remove-btn').disabled = false;
    }else if(parseInt(el.value) <= 1 || isNaN(parseInt(el.value))){
        el.value = 1;
        el.parentElement.querySelector('.card__remove-btn').disabled = true;
        el.parentElement.querySelector('.card__add-btn').disabled = false;
    }else if(el.value === ''){console.log('here')}else{
        el.value = parseInt(el.value);
        el.parentElement.querySelector('.card__add-btn').disabled = false;
        el.parentElement.querySelector('.card__remove-btn').disabled = false;
    }
}

function disableGoodsBtn(){
    const removeItemBtns = document.querySelectorAll('.card__remove-btn');
    removeItemBtns.forEach(e => e.disabled = true);
}

function initializeSlider(){
    const opts = {

        grid: {
            rows: 2,
            fill: "row",
        },

        pagination: {
            el: ".swiper-pagination",
            type: "custom",
            renderCustom: function (swiper, current, total) {
                return current + ' из ' + total;
            },
        },
        navigation: {
            nextEl: ".slider-nav__btn--next",
            prevEl: ".slider-nav__btn--prev",
        },
        breakpoints: {
            320: {
                slidesPerView:1,
                spaceBetween: 10,
            },
            380: {
                slidesPerView:2,
                slidesPerGroup:2,

            },
            700: {
                slidesPerView:3,
                slidesPerGroup:3,
                spaceBetween: 20,
            }
        }
    }
    new Swiper('.offers__slider', opts);
}

window.addEventListener('load', disableGoodsBtn);

window.addEventListener('load', initializeSlider);
window.addEventListener('resize', initializeSlider);

