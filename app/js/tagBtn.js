function handleTagContainer(){
    const btn = document.querySelector('.offers__tags-btn');
    const btnStyle = btn.currentStyle || window.getComputedStyle(btn);
    const btnLeftMargin = parseInt(btnStyle.marginLeft.slice(0, -2));
    const btnWidth = btn.offsetWidth + btnLeftMargin;
    const items = document.querySelectorAll('.offers__tag');
    const itemsContainer = document.querySelector('.offers__tags');
    let lineMaxWidth = itemsContainer.offsetWidth;
    lineMaxWidth -= btnWidth;
    let tagItemsTotal = 0;
    const delimiter = document.createElement('div');
    delimiter.classList.add('offers__tag-delimiter');

    for (const [k, it] of Object.entries(items)){
        btn.style.opacity = '1';
        btn.disabled = false;
        tagItemsTotal += it.offsetWidth;

        if ((tagItemsTotal > lineMaxWidth) && (k !== '0')){
            [...document.getElementsByClassName('offers__tag-delimiter')].forEach(e => {e.remove();});
            [...document.getElementsByClassName('offers__tag')].forEach(e => {e.style.marginBottom = '10px'});
            itemsContainer.insertBefore(delimiter, it);
            break;
        } else if((k === '0') && (tagItemsTotal > lineMaxWidth)){
            [...document.getElementsByClassName('offers__tag-delimiter')].forEach(e => {e.remove();});
            itemsContainer.insertBefore(delimiter, itemsContainer.querySelector('.offers__tag'));
            [...document.getElementsByClassName('offers__tag')].forEach(e => {e.style.marginBottom = '10px'});
            delimiter.style.height = btn.offsetHeight + 'px';
            break;
        }else if ((+k === items.length - 1) && (tagItemsTotal < lineMaxWidth)){
            [...document.getElementsByClassName('offers__tag')].forEach(e => {e.style.marginBottom = '0px'});
            [...document.getElementsByClassName('offers__tag-delimiter')].forEach(e => {e.remove();});
            btn.style.opacity = '0';
            btn.disabled = true;
            break;
        }
    }
}

function handleTagBtn(){
    const btn = document.querySelector('.offers__tags-btn');
    btn.addEventListener('click', function(){
        const container = this.parentElement;
        btn.classList.toggle('active');
        if (container.offsetHeight < container.scrollHeight){
            container.style.maxHeight = container.scrollHeight + 'px';
        }else{
            container.style.maxHeight = btn.offsetHeight + 'px';
        }
    });
}

window.addEventListener('load', handleTagBtn);

function setBtnContainerHeight(){
    const btn = document.querySelector('.offers__tags-btn');
    const container = document.querySelector('.offers__tags-wrapper');
    const tagHeight = document.querySelector('.offers__tag').offsetHeight;
    container.style.maxHeight = tagHeight  + 'px' ;
    btn.style.height = tagHeight + 'px';
    btn.classList.remove('active');
}

window.addEventListener('load', setBtnContainerHeight);


window.addEventListener('load', handleTagContainer);

var timeOutFunctionId;

function runAfterResize() {
    setBtnContainerHeight();
    handleTagContainer();
}

window.addEventListener("resize", function () {
    clearTimeout(timeOutFunctionId);
    timeOutFunctionId = setTimeout(runAfterResize, 500);

});

