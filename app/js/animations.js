function showHeroElements(){
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting){
                setTimeout(() => {
                    entry.target.classList.add('normalize-element');
                }, sleep += 450);
                setTimeout(() => {
                    document.querySelector('.hero__more-btn').classList.add('normalize-element');
                }, 2000);
            }
        })
    }
    const opts = {};
    const observer = new IntersectionObserver(cb, opts);
    const elems = document.querySelectorAll('.hero__title, .hero__desc, .hero__img-wrapper');
    elems.forEach(elem => {
        observer.observe(elem);
    });
}
showHeroElements();

function showCategoriesCards() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting){
                if(window.innerWidth > 330){
                    setTimeout(() => {
                        entry.target.classList.add('normalizeY-element');
                    }, sleep += 100);
                }else{
                    entry.target.classList.add('normalizeY-element');
                }
            }
        });
    };
    const opts = {threshold: .7};
    const observer = new IntersectionObserver(cb, opts);
    const elems = document.querySelectorAll('.categories__card');
    elems.forEach(elem => {
        observer.observe(elem);
    });
}
showCategoriesCards();

function showAdvantagesCards() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting){
                if (window.innerWidth > 576){
                    setTimeout(() => {
                        entry.target.classList.add('normalizeY-element');
                    }, sleep += 100);
                }else{
                    setTimeout(() => {
                        entry.target.classList.add('normalize-element');
                    }, sleep += 250);
                }
            }
        });
    };
    const opts = {threshold: .7};
    const observer = new IntersectionObserver(cb, opts);
    const elems = document.querySelectorAll('.advantages__card');
    elems.forEach(elem => {
        observer.observe(elem);
    });
}
showAdvantagesCards();

function showExpertsElements() {
    let sleep = 250;
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting){
                if (window.innerWidth > 576){
                    setTimeout(() => {
                        entry.target.classList.add('normalizeY-element');
                    }, sleep += 100);
                }else{
                    setTimeout(() => {
                        entry.target.classList.add('normalize-element');
                    }, sleep += 250);
                }
            }
        });
    };
    const opts = {threshold: .7};
    const observer = new IntersectionObserver(cb, opts);
    const elems = document.querySelectorAll('.advantages__card');
    elems.forEach(elem => {
        observer.observe(elem);
    });
}
showExpertsElements();

function showH2Title() {
    const cb = (entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting){
                entry.target.classList.add('normalizeX-element');
            }
        });
    };
    const opts = {threshold:.5};
    const observer = new IntersectionObserver(cb, opts);
    const elems = document.querySelectorAll('h2, .offers__title-wrapper');
    elems.forEach(elem => {
        observer.observe(elem);
    });
}
showH2Title();

