function toggleAccordion(){
    const acc = document.getElementsByClassName("accordion__header");
    const panels = document.querySelectorAll('.accordion__body');


    [...acc].forEach((accordion, accIdx) => {
        accordion.addEventListener('click', function(){
            panels.forEach((panel, panelIdx) => {
                if(panelIdx !== accIdx){
                    panel.style.maxHeight = null;
                    panel.previousElementSibling.classList.remove('active');
                }else{
                    panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
                    panel.previousElementSibling.classList.toggle('active');
                }
            })
        });
    });
}

toggleAccordion();